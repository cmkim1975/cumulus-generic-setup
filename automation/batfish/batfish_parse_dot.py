#!/usr/bin/env python

import re
import json

'''
 "pop-sp1":"swp1" -- "pop-z09-le1":"swp56"

{
  "edges": [
    {
      "node1": {
        "hostname": "pop-sp1",
        "interfaceName": "swp1"
      },
      "node2": {
        "hostname": "pop-z09-le1",
        "interfaceName": "swp56"
      },
    }
  ]
}
'''

output = {}
output['edges'] = []

file = open('topology.dot', 'r')
for i in file:
  m = re.match(r'^ "(\S+)\"\:\"(\S+)\" -- \"(\S+)\"\:\"(\S+)\"', i)
  if m:
    entry = {}
    entry['node1'] = {}
    entry['node1']['hostname'] = m.group(1)
    entry['node1']['interfaceName'] = m.group(2)
    entry['node2'] = {}
    entry['node2']['hostname'] = m.group(3)
    entry['node2']['interfaceName'] = m.group(4)
    output['edges'].append(entry)

with open ('layer1_topology.json', 'w') as file_write:
  json.dump(output, file_write, sort_keys=True, indent=2, separators=(',', ': '))
